import React, { Component } from "react";
import axios from "axios";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import "./App.css";
import "bootstrap/dist/css/bootstrap.css";
import "bootstrap/dist/js/bootstrap.js";
import Nav from "./components/Nav";
import Products from "./components/Products";
import NewProduct from "./components/NewProduct";
import EditProduct from "./components/EditProduct";
import ViewProduct from "./components/ViewProduct";
import NoMatch from "./components/NoMatch";
import Loader from "./components/Loader";
import DeleteConfirmation from "./components/DeleteConfirmation";

class App extends Component {
    constructor(props) {
        super(props);

        this.API_STATES = {
            LOADING: "loading",
            LOADED: "loaded",
            ERROR: "error",
        };

        this.state = {
            products: [],
            product: {},
            status: this.API_STATES.LOADING,
            errorMessage: "",
        };

        this.URL = "https://fakestoreapi.com/products";
    }

    fetchData = (url) => {
        this.setState(
            {
                status: this.API_STATES.LOADING,
            },
            () => {
                axios
                    .get(url)
                    .then((response) => {
                        this.setState({
                            status: this.API_STATES.LOADED,
                            products: response.data,
                        });
                    })
                    .catch((error) => {
                        this.setState({
                            status: this.API_STATES.ERROR,
                            errorMessage:
                                "An API error occurred. Please try again in a few minutes.",
                        });
                    });
            }
        );
    };

    getProductById = (id) => {
        let product = this.state.products.filter((product) => {
            return product.id === Number(id);
        });

        product = product.length ? product[0] : {};

        this.setState(
            {
                product,
            },
            () => {
                console.log(this.state);
            }
        );
    };

    generateNewId = () => {
        let newId =
            this.state.products.reduce((maxIdvalue, product) => {
                if (product.id > maxIdvalue) {
                    maxIdvalue = product.id;
                }
                return maxIdvalue;
            }, 0) + 1;
        return newId;
    };

    deleteProduct = (productToDelete) => {
        let products = this.state.products;

        products = products.filter((product) => {
            return product.id !== productToDelete.id;
        });

        this.setState({
            products,
        });
    };

    AddProduct = (productToAdd) => {
        let products = this.state.products;

        products.push(productToAdd);

        this.setState(
            {
                products,
            },
            () => {
                console.log("Add", this.state);
            }
        );
    };

    UpdateProduct = (productToUpdate) => {
        let products = this.state.products.map((product) => {
            if (product.id === productToUpdate.id) {
                return productToUpdate;
            } else {
                return product;
            }
        });

        this.setState(
            {
                products,
                product: productToUpdate,
            },
            () => {
                console.log("Add", this.state);
            }
        );
    };

    componentDidMount = () => {
        this.fetchData(this.URL);
    };

    render() {
        return (
            <Router>
                <div className="App">
                    <header className="App-header">
                        <Nav />
                    </header>

                    <DeleteConfirmation
                        product={this.state.product}
                        deleteProduct={this.deleteProduct}
                    />

                    {this.state.status === this.API_STATES.LOADING && (
                        <Loader />
                    )}

                    {this.state.status === this.API_STATES.ERROR && (
                        <div className="error">
                            <h1>{this.state.errorMessage}</h1>
                        </div>
                    )}

                    {this.state.status === this.API_STATES.LOADED && (
                        <Switch>
                            <Route exact path="/">
                                <Products
                                    products={this.state.products}
                                    getProductById={this.getProductById}
                                />
                            </Route>

                            <Route exact path="/admin">
                                <Products
                                    products={this.state.products}
                                    getProductById={this.getProductById}
                                />
                            </Route>

                            <Route exact path="/admin/new">
                                <NewProduct
                                    newId={this.generateNewId()}
                                    AddProduct={this.AddProduct}
                                />
                            </Route>

                            <Route
                                path="/edit/:id"
                                render={(props) => {
                                    return (
                                        <EditProduct
                                            {...props}
                                            product={this.state.product}
                                            getProductById={this.getProductById}
                                            UpdateProduct={this.UpdateProduct}
                                        />
                                    );
                                }}
                            />
                            <Route
                                path="/view/:id"
                                render={(props) => {
                                    return (
                                        <ViewProduct
                                            {...props}
                                            product={this.state.product}
                                            getProductById={this.getProductById}
                                        />
                                    );
                                }}
                            />

                            <Route path="*" component={NoMatch} />
                        </Switch>
                    )}
                </div>
            </Router>
        );
    }
}

export default App;
