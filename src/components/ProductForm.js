import React, { Component } from "react";
import { Link } from "react-router-dom";
import Joi from "joi";

import FormConfirmation from "./FormConfirmation";

class ProductForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            product: {
                title: "",
                category: "",
                price: null,
                description: "",
                image: "",
                rating: {
                    rate: 0,
                    count: 0,
                },
            },
            errors: {},
            isSubmitted: false,
        };

        this.schema = Joi.object({
            id: Joi.number(),
            title: Joi.string().required().label("Name"),
            category: Joi.string().required(),
            description: Joi.string().required(),
            price: Joi.number().min(0).required(),
            image: Joi.string().uri().required().label("Image url"),
            rating: {
                rate: Joi.number(),
                count: Joi.number(),
            },
        });
    }

    validateProperty = (product) => {
        const result = this.schema.validate(product, {
            abortEarly: false,
        });
        const { error } = result;

        return error ? error.details : null;
    };

    handleInputChange = (event) => {
        const target = event.target;
        const value = target.value;
        const name = target.name;

        const product = { ...this.state.product };
        const errors = { ...this.state.errors };

        if (errors.hasOwnProperty(name)) {
            delete errors[name];
        }

        product[name] = value;
        const errorsData = this.validateProperty(product);

        if (errorsData) {
            errorsData.forEach((error) => {
                if (error.path[0] === name) {
                    errors[name] = error.message;
                }
            });
        }

        this.setState({
            product,
            errors,
        });
    };

    handleFormSubmit = (event) => {
        event.preventDefault();

        const product = { ...this.state.product };
        const errorsTemp = {};
        let isSubmitted = this.state.isSubmitted;

        const errorsData = this.validateProperty(product);

        if (errorsData) {
            errorsData.forEach((error) => {
                const name = error.path[0];
                errorsTemp[name] = error.message;
            });
        }

        isSubmitted = Object.keys(errorsTemp).length === 0;

        if (isSubmitted) {
            this.props.AddProduct(product);
        }

        this.setState(
            {
                product,
                errors: errorsTemp,
                isSubmitted,
            },
            () => {
                console.log(this.state);
            }
        );
    };

    componentDidMount() {
        const product = this.props.product;
        this.setState({
            product: { ...product },
        });
    }

    render() {
        const { product, errors, isSubmitted } = this.state;

        return (
            <>
                {isSubmitted ? (
                    <FormConfirmation message={this.props.successMessage} />
                ) : (
                    <div className="main-content container">
                        <div className="title d-flex mb-3">
                            <Link to="/admin">
                                <button className="btn btn-primary">
                                    Back
                                </button>
                            </Link>
                            <h1 className="m-auto">Product Details</h1>
                        </div>
                        <form id="productEditForm">
                            <div className="container card shadowed p-4">
                                <div className="row g-3 gx-5">
                                    <div className="title mb-2">
                                        <label
                                            htmlFor="name"
                                            className="form-label"
                                        >
                                            Name
                                        </label>
                                        <input
                                            type="text"
                                            className={
                                                "form-control" +
                                                " " +
                                                (errors.title
                                                    ? "is-invalid"
                                                    : product.title
                                                    ? "is-valid"
                                                    : "")
                                            }
                                            name="title"
                                            value={product.title || ""}
                                            placeholder="Enter the product name"
                                            onChange={this.handleInputChange}
                                            id="name"
                                        />
                                        <div className="invalid-feedback">
                                            {errors.title}
                                        </div>
                                    </div>
                                    <div className="category mb-2">
                                        <label
                                            htmlFor="category"
                                            className="form-label"
                                        >
                                            Category
                                        </label>
                                        <input
                                            type="text"
                                            className={
                                                "form-control" +
                                                " " +
                                                (errors.category
                                                    ? "is-invalid"
                                                    : product.category
                                                    ? "is-valid"
                                                    : "")
                                            }
                                            name="category"
                                            value={product.category || ""}
                                            placeholder="Enter the product category"
                                            onChange={this.handleInputChange}
                                            id="category"
                                        />
                                        <div className="invalid-feedback">
                                            {errors.category}
                                        </div>
                                    </div>
                                    <div className="price mb-2">
                                        <label
                                            htmlFor="price"
                                            className="form-label"
                                        >
                                            Price
                                        </label>
                                        <input
                                            type="number"
                                            className={
                                                "form-control" +
                                                " " +
                                                (errors.price
                                                    ? "is-invalid"
                                                    : product.price
                                                    ? "is-valid"
                                                    : "")
                                            }
                                            name="price"
                                            value={product.price || 0}
                                            placeholder="Enter the product price"
                                            onChange={this.handleInputChange}
                                            id="price"
                                        />
                                        <div className="invalid-feedback">
                                            {errors.price}
                                        </div>
                                    </div>
                                    <div className="description mb-2">
                                        <label
                                            htmlFor="description"
                                            className="form-label"
                                        >
                                            Description
                                        </label>
                                        <textarea
                                            className={
                                                "form-control" +
                                                " " +
                                                (errors.description
                                                    ? "is-invalid"
                                                    : product.description
                                                    ? "is-valid"
                                                    : "")
                                            }
                                            name="description"
                                            value={product.description || ""}
                                            placeholder="Enter the product description"
                                            onChange={this.handleInputChange}
                                            id="description"
                                            rows="4"
                                        />
                                        <div className="invalid-feedback">
                                            {errors.description}
                                        </div>
                                    </div>
                                    <div className="image mb-2">
                                        <label
                                            htmlFor="image"
                                            className="form-label"
                                        >
                                            Image
                                        </label>
                                        <input
                                            type="url"
                                            className={
                                                "form-control" +
                                                " " +
                                                (errors.image
                                                    ? "is-invalid"
                                                    : product.image
                                                    ? "is-valid"
                                                    : "")
                                            }
                                            name="image"
                                            value={product.image || ""}
                                            placeholder="Enter the product image url"
                                            onChange={this.handleInputChange}
                                            id="image"
                                        />
                                        <div className="invalid-feedback">
                                            {errors.image}
                                        </div>
                                        {product.image && !errors.image && (
                                            <div className="text-center mt-2">
                                                <img
                                                    id="image-preview"
                                                    className="image-preview"
                                                    src={product.image}
                                                    alt=""
                                                />
                                            </div>
                                        )}
                                    </div>
                                </div>
                                <div className="form-submit-button d-flex justify-content-around mt-4">
                                    <Link to="/admin">
                                        <button
                                            type="button"
                                            className="btn btn-danger"
                                        >
                                            Cancel
                                        </button>
                                    </Link>
                                    <button
                                        type="button"
                                        className="btn btn-warning"
                                        onClick={this.handleFormSubmit}
                                    >
                                        Save
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                )}
            </>
        );
    }
}

export default ProductForm;
