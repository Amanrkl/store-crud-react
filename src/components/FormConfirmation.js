import React, { Component } from "react";
import { Link } from "react-router-dom";

class FormConfirmation extends Component {
    render() {
        return (
            <div className="container">
                <div className="card p-2 text-center">
                    <div className="header">
                        <h3 className="title">Form Status</h3>
                    </div>
                    <div className="body">
                        <p>The product was {this.props.message} successfully</p>
                    </div>
                    <Link to="/admin"
                    >
                        <button
                            type="button"
                            className="btn btn-secondary"
                        >
                            OK
                        </button>
                    </Link>
                </div>
            </div>
        );
    }
}

export default FormConfirmation;
