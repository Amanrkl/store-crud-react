import React, { Component } from 'react';

import ProductForm from './ProductForm';
import '../assets/style/editProduct.css';

class EditProduct extends Component {
    componentDidMount() {
        const { match: { params } } = this.props;
        this.props.getProductById(params.id);
    }

    render() {
        const { product } = this.props;

        return (product.id === Number(this.props.match.params.id) &&
            <ProductForm
                product={product}
                AddProduct={this.props.UpdateProduct}
                successMessage='updated'
            />
        );
    }
}

export default EditProduct;
