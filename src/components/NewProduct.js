import React, { Component } from "react";

import ProductForm from "./ProductForm";

class NewProduct extends Component {
    render() {
        console.log("new", this.props);
        return (
            <ProductForm
                product={{
                    id: this.props.newId,
                    rating: {
                        rate: 0,
                        count: 0
                    },
                }}
                AddProduct={this.props.AddProduct}
                successMessage='created'
            />
        );
    }
}

export default NewProduct;
