import React, { Component } from "react";
import { Link } from "react-router-dom";

import "../assets/style/viewProduct.css";

class ViewProduct extends Component {
    componentDidMount() {
        const {
            match: { params },
        } = this.props;
        this.props.getProductById(params.id);
    }

    render() {
        const { product } = this.props;

        return (
            product.id === Number(this.props.match.params.id) && (
                <div className="container mt-5 mb-5">
                    <div className="row d-flex justify-content-center">
                        <div className="col-md-10">
                            <div className="card">
                                <div className="row">
                                    <div className="col-md-6">
                                        <div className="images p-3">
                                            <div className="text-center p-4">
                                                <img
                                                    id="main-image"
                                                    src={product.image}
                                                    alt=""
                                                    width="250"
                                                />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                        <div className="product p-4 h-100">
                                            <div className="mt-4 mb-3">
                                                <p className="text-uppercase text-muted mb-2">
                                                    {product.category}
                                                </p>
                                                <h5 className="text-uppercase mb-4">
                                                    {product.title}
                                                </h5>
                                                <h6 className="act-price mb-2">
                                                    ${product.price}
                                                </h6>
                                                {product.rating.rate > 0 && (
                                                    <p className="mt-2">
                                                        {product.rating.rate} (
                                                        {product.rating.count})
                                                    </p>
                                                )}
                                            </div>
                                            <p className="about">
                                                {product.description}
                                            </p>
                                            <div className="product-button d-flex gap-3">
                                                <Link to="/admin">
                                                    <button className="btn btn-primary">
                                                        Back
                                                    </button>
                                                </Link>
                                                <Link
                                                    to={`/edit/${product.id}`}
                                                >
                                                    <button className="btn btn-warning">
                                                        Update
                                                    </button>
                                                </Link>
                                                <Link to="/admin">
                                                    <button
                                                        className="btn btn-danger"
                                                        data-bs-toggle="modal"
                                                        data-bs-target="#deleteModal"
                                                    >
                                                        Delete
                                                    </button>
                                                </Link>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            )
        );
    }
}

export default ViewProduct;
