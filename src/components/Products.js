import React, { Component } from "react";
import { Link } from "react-router-dom";

import Product from "./Product";
import "../assets/style/products.css";

class Products extends Component {
    constructor(props) {
        super(props);

        this.state = {
            products: [],
        };
    }

    render() {
        const { products } = { ...this.props };

        return (
            <>
                {products.length === 0 && (
                    <div className="error">
                        <h1>Currently No products available.</h1>
                        <button className="btn btn-primary">
                            Add new Product
                        </button>
                    </div>
                )}

                {products.length > 0 && (
                    <div className="container">
                        <div className="table-responsive">
                            <div className="table-wrapper">
                                <div className="table-title">
                                    <div className="row">
                                        <div className="col-sm-10">
                                            <h2>Products</h2>
                                        </div>
                                        <div className="col-sm-2">
                                            <Link to="/admin/new">
                                                <button className="btn btn-primary">
                                                    Add new Product
                                                </button>
                                            </Link>
                                        </div>
                                    </div>
                                </div>
                                <table className="table table-hover table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Sl no.</th>
                                            <th>Image</th>
                                            <th>Name</th>
                                            <th>Category</th>
                                            <th>Rating</th>
                                            <th>Price</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {products.map((product, index) => {
                                            return (
                                                <Product
                                                    key={product.id}
                                                    no={index + 1}
                                                    product={product}
                                                    deleteProduct={
                                                        this.props
                                                            .getProductById
                                                    }
                                                />
                                            );
                                        })}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                )}
            </>
        );
    }
}

export default Products;
