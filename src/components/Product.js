import React, { Component } from 'react';
import { Link } from "react-router-dom";

import '../assets/style/product.css';

class Product extends Component {

    render() {
        const product = this.props.product;
        const no = this.props.no;

        return (
            <tr>
                <td>{no}</td>
                <td>
                    <div className="product-image">
                        <img
                            src={product.image}
                            alt="" />
                    </div>
                </td>
                <td>{product.title}</td>
                <td>{product.category}</td>
                <td>{product.rating.rate} ({product.rating.count})</td>
                <td>${product.price}</td>
                <td>
                    <Link
                        to={`/view/${product.id}`}
                        className="view"
                        title="View"
                        data-toggle="tooltip"
                    >
                        <i className="fa-solid fa-eye" />
                    </Link>
                    <Link
                        to={`/edit/${product.id}`}
                        className="edit"
                        title="Edit"
                        data-toggle="tooltip"
                    >
                        <i className="fa-solid fa-pen" />
                    </Link>
                    <Link
                        to='/admin'
                        className="delete"
                        title="Delete"
                        data-bs-toggle="modal"
                        data-bs-target="#deleteModal"
                        onClick={() => this.props.deleteProduct(product.id)}>
                        <i className="fa-solid fa-trash-can"></i>
                    </Link>
                </td>
            </tr>
        );
    }
}

export default Product;